const alert_container = document.querySelector("#alert_container");


function removeAlert()
{
        
	setTimeout(function() {
		alert_container.innerHTML = "";
		
    }, 3000);
    // $(document).ready(function(){
    //     $('.alert-success').fadeIn().delay(10000).fadeOut();
    //       });
}

function displaySuccessMessage(content)
{
	let alert_success = document.createElement("div");
	alert_success.setAttribute("class", "alert alert-success");
	alert_success.setAttribute("id", "alert_success");
	alert_success.textContent = content;
	alert_container.append(alert_success);
    removeAlert();
    

    
    
}

function displayErrorMessage(content)
{
	let alert_error = document.createElement("div");
	alert_error.setAttribute("class", "alert alert-danger");
	alert_error.setAttribute("id", "alert_error");
	alert_error.textContent = content;
	alert_container.append(alert_error);
	removeAlert();
}


document.addEventListener("click",function(e){
    // =========== Delete Event=================//
    if(e.target.classList.contains("btn-delete-event")) {
        let button = e.target;
        let event_id = button.dataset.id;
        let event_name = button.dataset.name;
        
        let modal_text = document.querySelector("#delete_event_name");
        modal_text.innerHTML = event_name;

        let modal_form = document.querySelector("#delete_modal_form");
        // console.log(modal_form);
        modal_form.setAttribute("action", `/events/${event_id}`);
        
    }
    // ============payment received confirmation ================//
    if(e.target.classList.contains("btn-confirm-payment")){
        // alert("test");
        let button = e.target;
        let user_id = button.dataset.id;
        let user_name = button.dataset.name;
        // console.log(user_name)

        let modal_text = document.querySelector("#user_name");
        modal_text.innerHTML = user_name;
        let modal_form = document.querySelector("#paymentConfirmation_modal_form");
        modal_form.setAttribute("action", `/paymentConfirmation/${user_id}`);
    }

});

// ================ SEARCH ===============
let inputSearch = document.querySelector("#search");
inputSearch.addEventListener("keydown", function(e){
    if(e.keyCode==13){
        e.preventDefault();
        let searchKey = inputSearch.value;

        let form = document.querySelector("#search-form");
        
        fetch(`/search`, {
            method: "POST",
            body: new FormData(form),
            credentials: 'same-origin'
        })
        .then(function(response){
            return response.text();
        })
        .then(function(data_from_fetch){
            if(data_from_fetch != "none") {
                let container = document.querySelector(".event-container");
                container.innerHTML = data_from_fetch;
            } else {
                let content = `Sorry ${searchKey} didn't match any posted event`;
                displayErrorMessage(content);
            }
        })
        .catch(function(error){
            console.log("error", error);
        })
    }
});