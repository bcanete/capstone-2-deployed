<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/','PageController@index');
Route::post('/search','PageController@search');
Route::get('/event/{id}/view', 'PageController@viewEvent');
Route::get('/event/create', 'EventController@create');
Route::post('/event/createSubmitted','EventController@store');
Route::get('/event/{id}/edit', 'EventController@edit');
Route::put('/event/{id}', 'EventController@update');
Route::delete('/events/{id}','EventController@destroy');
Route::get("/restore/{id}", "EventController@restore");


Route::post('/confirmation/{id}','EventParticipantController@confirmation');
Route::get('/event/{id}/participantsList', "PageController@viewParticipantsList");
Route::put('/paymentConfirmation/{id}', 'EventParticipantController@payment_received');

Route::group(["middleware" => "auth"], function(){
    Route::get('event/{id}/participant', 'EventParticipantController@index');
    Route::get('/event/list', "PageController@viewEventList");
});
