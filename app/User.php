<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name','name', 'email','contact_number', 'password','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function events(){
        //users belongsToMany Events
        return $this->belongsToMany('App\Event');
    }

    public function event(){
        //user has many Events
        return $this->hasMany('App\Event');
    }

    public function role(){
        // a user belongsTo single Role
        return $this->belongsTo('App\Role', 'event_participants')->withTimestamps();
    }
}
