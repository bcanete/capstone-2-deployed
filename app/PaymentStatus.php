<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    //a paymentStatus hasMany Transactions
    public function transactions(){
    	return $this->hasMany('App\Transaction');
    }
}
