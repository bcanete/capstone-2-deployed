<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMode extends Model
{
    //a paymentMode hasMany Transactions
    public function transactions(){
    	return $this->hasMany('App\Transaction');
    }
}
