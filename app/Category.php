<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    //a category hasMany Events
    public function events(){
    	return $this->hasMany('\App\Event');
    }
}
