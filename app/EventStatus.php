<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventStatus extends Model
{
    //a status hasMany Events
    public function events(){
    	return $this->hasMany('\App\Event');
    }
}
