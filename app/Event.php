<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes; 
    
    //an event belongsTo only one category
    public function category(){
        return $this->belongsTo('App\Category');
    }
    // an event belongsTo single status 
    public function eventStatus(){
    	return $this->belongsTo('App\EventStatus');
    }
    //event belongsToMany Users
    public function users(){
    	return $this->belongsToMany('App\User', 'event_participants')->withTimestamps();
    }

    //event has one owner
    public function owner()
    {
        return $this->belongsTo("App\User", "user_id");
    }
}
