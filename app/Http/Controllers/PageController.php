<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Event;
use App\User;
use App\Category;
use Auth;
use App\Transaction;
use App\PaymentMode;
use App\PaymentStatus;

class PageController extends Controller
{
    public function index(Request $request){
		// dd(Auth::user()->name);
		$title = "Upcoming Events";
		$categories = Category::all();
		$events = Event::OrderBy("date","desc")->paginate(5);
		// dd('test');
		$user = User::all();
		$old_category = null;

		if(is_numeric($request->category)){
			$events = $this->filter($request);
			$old_category = $request->category;
		} else {
			$events = Event::orderBy("name")->paginate(5);
		}

    	// return view('welcome')->with('events','title',$events, $title);
    	return view("welcome",compact('title','events','user', 'categories','old_category'));
	}

	public function filter(Request $request){
		if(is_numeric($request->category)){
			$events = Event::where("category_id", $request->category)->paginate(5);
		}
		return $events;
	}

    public function viewEvent($id){
		
    	$title="Event Details";
    	$event=Event::find($id);

    	return view("events.viewEvent", compact("title","event"));

    }

    public function viewEventList(){
		// dd(Auth::user()->role_id);
		$title = "My Events";
		if(Auth::user()->role_id == 1){
			$events = Event::OrderBy("date","desc")->paginate(5);
			return view("events.eventList",compact('title','events',));
		}
		elseif(Auth::user()->role_id == 2){
			$user_id = Auth::user()->id;
			$events = Event::where("user_id", $user_id)->OrderBy("date","desc")->paginate(5);
			return view("events.eventList",compact('title','events',));
		}
		elseif(Auth::user()->role_id == 3){
			$user_id = Auth::user()->id;
			$events = [];
			// dd($user);
			$event_participant = DB::table('event_participants')->where('user_id', $user_id)->get();
			// dd($event_participant);

			foreach($event_participant as $e){
				// dd($e->id);
				$event = Event::find($e->event_id);
				// dd($event);
				$transaction = Transaction::where("event_participants_id", $e->id)->first();
				// dd($transaction);
				// dd(PaymentMode::find(1));
				$payment_mode = PaymentMode::find($transaction->payment_mode_id);
				$event->payment_mode = $payment_mode->name;
				$payment_status = PaymentStatus::find($transaction->payment_status_id);
				$event->payment_status = $payment_status->name;
				$events[] = $event;
				
			}				
			return view("events.userEventList",compact('title','events',));
		}	
	}

	public function search(Request $request){
		// dd("test");
		$searchKey = $request->search;
		$events = Event::where("name", "like", "%" . $searchKey . "%")->paginate(5);
		
		// dd($events->count());
		if($events->count() > 0){
			return view("partials.search", compact("events"));
		} else{
			return "none";
		}
	}
	
	
	public function viewParticipantsList($id){
		$title = "List of participants";
		$payment_statuses = PaymentStatus::all();
		// dd($payment_status);
		$event_id = $id;
		$users = [];		
		$event_participant = DB::table('event_participants')->where('event_id', $event_id)
		->get();
		// dd($event_participant->event_id);

		foreach($event_participant as $e){
			$user = User::find($e->user_id);
			// dd($user->contact_number);
			$transaction = Transaction::where("event_participants_id", $e->id)->first();
			$payment_mode = PaymentMode::find($transaction->payment_mode_id);
			$user->payment_mode = $payment_mode->name;
			$payment_status = PaymentStatus::find($transaction->payment_status_id);
			$user->payment_status = $payment_status->name;
		
			$users[] = $user;

		}
		// dd($transaction);
		
		return view("participants.event_participants_list", compact("title", "users", "payment_statuses","transaction"));

	}

}
