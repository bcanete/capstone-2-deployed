<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\Event;
use Auth;
use App\PaymentMode;
use App\Transaction;
use Illuminate\Support\Facades\DB;


class EventParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $title = "Joiners Information";
        $user = Auth::user();
        $event = Event::find($id);
        $payment_modes = PaymentMode::all();

        return view("participants.joiners_info", compact("event","title","user","payment_modes"));
    }

    public function confirmation(Request $request, $id){
        $user = Auth::user();
        $event = Event::find($id);
        $event->users()->attach($user->id);
        $event_participant = DB::table("event_participants")
        ->where("user_id", $user->id)
        ->latest()
        ->first();
        
        $transaction = new Transaction;
        $transaction->transaction_code = Str::random(5) . " - " . time();
        $transaction->event_participants_id = $event_participant->id;
        $transaction->payment_mode_id = intval($request->input("payment-mode"));
        
        $transaction->payment_status_id = 4;

        $transaction->save();

        $title="Confirmation";
        // $transactions = Transaction::(all);
        // dd($transaction->payment_mode->name);
        return view("participants.confirmation",compact("transaction","title","event")); 

    }

    public function payment_received(Request $request, $id){
        $new_payment_status_id = $request->payment_status;
        // dd($new_payment_status_id);
        $user_id = $id;
        // dd($user_id);
        $event_participant = DB::table('event_participants')->where('user_id', $user_id)->get();
        
        // dd($event_participant);
        foreach($event_participant as $e ){
            $transaction = Transaction::where("event_participants_id", $e->id)->first();
        }
        
        // dd($transaction);
        $transaction->payment_status_id = $new_payment_status_id;
        // dd($transaction);
        // dd($new_payment_status_id);
        $transaction->save();
        // dd($transaction);
        return redirect()->back()->with("success", "Payment Confirmed");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
