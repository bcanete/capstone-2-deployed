<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Category;
use App\EventStatus;
use Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.createEvent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation
        $this->validate($request,[
            'name' => "required",
            'location' => 'required',
            'date' => "required|date",
            'time'=> "required",
            'price'=> "required|numeric",
            'target_slots'=> "required|numeric",
            'category' => "required|numeric",
            'image' => "required",
            'details' => "required",
        ]);

        $event = new Event;
        $event->name = $request->input("name");
        // dd("$event->name");
        $event->location = $request->input("location");
        $event->date = $request->input("date");
        $event->time = $request->input("time");
        $event->Price = $request->input("price");
        $event->target_slots = $request->input("target_slots");
        $event->category_id = $request->input("category");

        $event->details = strip_tags($request->input("details"));
        $event->user_id = Auth::user()->id;

        $image = $request->file("image");
            // set image name
            $image_name = time() . "." . $image->getClientOriginalExtension();

            $destination = "images/";
            $image->move($destination, $image_name);
            $event->image = "/" . $destination . "/" . $image_name;

            $event->save();
            return redirect()->back()->with("success", "Event Created");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $title="Edit Events Page";
        $events = Event::find($id);
        $categories = Category::all();
        $status = EventStatus::all();

        return view("events.editEvent",compact("title", "events", "categories","status"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation
        $this->validate($request,[
            'name' => "required",
            'location' => 'required',
            'date' => "required|date",
            'time'=> "required",
            'price'=> "required|numeric",
            'target-slots'=> "required|numeric",
            'category' => "required|numeric",
            'status' => "required|numeric",
            'details' => "required",
        ]);

        $event = Event::find($id);
        $event->name = $request->input("name");
        // dd($event->name);
        $event->location = $request->input("location");
        $event->date = $request->input("date");
        $event->time = $request->input("time");
        $event->Price = $request->input("price");
        $event->target_slots = $request->input("target-slots");
        $event->category_id = $request->input("category");
        $event->event_status_id = $request->input("status");
        $event->details = strip_tags($request->input("details"));

        $image = $request->file("image");
        // dd($request);

        if($image != null ){
            // set image name
            $image_name = time() . "." . $image->getClientOriginalExtension();

            
            $destination = "images/";               
            $image->move($destination, $image_name);
            $event->image = "/" . $destination . "/" . $image_name;

        
        }

        $event->save();
        return redirect()->back()->with("success", "Event Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $event = Event::find($id);
        $event_name = $event->name;
        $event->delete();
        return redirect("/event/list")
        ->with("success","$event_name has been deleted!")
        ->with("undo_url", "/restore/$id");
    }

    public function restore($id){
        // get deleted event
        
        $event = Event::onlyTrashed()
        ->where('id',$id)
        ->first();

        $event->restore();
        return back()->with("success","$event->name has been restored!");
    }
}
