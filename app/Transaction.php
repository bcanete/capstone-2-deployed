<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //an transactions belongsTo a single paymentmode
    public function paymentMode(){
    	return $this->belongsTo('App\PaymentMode');
    }

    //an transactions belongsTo a single paymentstatus
    public function paymentStatus(){
    	return $this->belongsTo('App\PaymentStatus');
    }
}
