<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //a role hasMany Users
    public function users() {
    	return $this->hasMany('App\User');
    }
}
