@extends("layouts.app")
@section("content")
<div class="row mb-4">
    <div class="col">
        <div class="display-4">
            {{ $title }}
        </div>
    </div>
</div>
<div class="row mb-4">
    <div class="col">
        <p>Your Confirmation code is <span class="font-weight-bold">{{ $transaction->transaction_code }}</span>.</p>
    </div>
</div>
<div class="row mb-4">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title font-weight-bold">You have successfully joined an event</h5>
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" width="20%">Event</th>
                            <th scope="col" width="20%">Date</th>
                            <th scope="col" width="20%">Time</th>
                            <th scope="col" width="20%">Location</th>
                            <th scope="col" width="20%">Price</th>
                            <th scope="col" width="20%">Payment Mode</th>
                            <th scope="col" width="20%">Payment Status</th>

                        </tr>
                    </thead>
                    <tbody>
                      
                      
                        <tr>
                       
                            
                            <td>{{$event->name}}</td>
                            <td>{{$event->date}}</td>
                            <td>{{$event->time}}</td>
                            <td>{{$event->location}}</td>
                            <td>{{$event->Price}}</td>
                           
                        
                            <td>
                                
                              
                                @switch($transaction->payment_mode_id)
                                    @case(1)
                                        Bank Transfer
                                        @break

                                    @case(2)
                                        GCash
                                        @break
                                    
                                    @case(2)
                                        LBC
                                        @break

                                    @default
                                        Others
                                @endswitch

                                  
                            </td>
                            <td>
                                
                                @switch($transaction->payment_status_id)
                                    @case(1)
                                        Down Payment
                                        @break

                                    @case(2)
                                        Fully Paid
                                        @break
                                    
                                    @case(2)
                                        Cancelled
                                        @break

                                    @default
                                        Pending
                                @endswitch
                            
                            </td>
                             
                     
                        </tr>                                                                 
                    </tbody>
                    <p>
                       Note: Please settle your payment and upload a picture of receipt or confirmation to be officially listed in this event. 
                    </p>
                </table>
            </div>
        </div>
    </div>
</div>


@endSection