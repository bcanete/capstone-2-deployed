@extends("layouts.app")

@section("content")
    <div class="row">
        <div class="col">
            <h2>{{$title}}</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <form method="POST" action="/confirmation/{{ $event->id }}" >
            @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                </div>
                <div class="form-group">
                    <label for="contact-number">Contact Number</label>
                    <input type="text" name="contact-number" placeholder="location" class="form-control" value="{{$user->contact_number}}">
                </div>
                <div class="form-group">
                    <label for=""></label>
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" name="email" placeholder="date" class="form-control" value="{{$user->email}}" >
                </div>
                <div class="form-group">
                    <label for="event-name">Event Name:</label>
                    <input type="text" name="event-name" placeholder="time" class="form-control" value="{{$event->name}}" disabled>
                </div>
                <div class="form-group">
                    <label for="price">Price:</label>
                    <input type="text" name="price" placeholder="price" class="form-control" value="{{$event->Price}}" disabled> 
                </div>
                
                
                <div class="form-group">
                    <label for="payment-mode">Preferred Payment Method: </label>
                    <select class="custom-select" name="payment-mode" 
                    id="payment-mode">
                        @foreach($payment_modes as $payment_mode)
                            <option value="{{ $payment_mode->id }}"
                                    {{ $payment_mode->id == $payment_mode->id 
                                        ? "selected" : "" }}
                                >
                                {{ $payment_mode->name }}
                            </option>
                        @endforeach
                    </select>
                        <small>Note: Payment Method Details can be found in details of event. </small>
                    
                </div>
                
                                        
                
                
                

                
            
        </div>
        <div class="col-6 m">
                <img src="{{ $event->image }}" class="img-fluid"><br><br>
                <h5>Details:</h5>
				<details>
					<summary>View Detials</summary>
					<pre>{{$event->details}}</pre>
				</details>    
        </div>
        <button class="btn btn-primary mt-2">Submit</button>
            </form>
            
        </div>
    </div>
@endsection