@extends("layouts.app")
@section("content")

<div class="col">
<h2>{{$title}}</h2>
    
		<table class="table table-striped">
		  <thead>
		    <tr>
			
		      <th scope="col" width="20%">#</th>
		      <th scope="col" width="20%">Name</th>
		      <th scope="col" width="20%">Email</th>
		      <th scope="col" width="20%">Contact Number</th>
		      <th scope="col" width="20%">Payment Mode</th>
		      <th scope="col" width="20%">Payment Status</th>
              <th><th scope="col" width="20%"></th></th>
              
		      
		    
		    </tr>
		  </thead>
		  <tbody>
            
             
		    <tr>
            @if(empty($users))
			{{-- dd($users) --}}
               <h2 class = "mt-5">No Participants have joined your event yet</h2>
			@else   
            
			
            
		  	@foreach($users as $user)
				<th scope="row">{{ $loop->iteration }}</th>
				<td>{{$user->name}}</td>
				<td>{{$user->email}}</td>
				<td>{{$user->contact_number}}</td>
				<td>{{$user->payment_mode}}</td>
				<td>
                
					{{--$transaction->payment_status_id--}}
					@switch($transaction->payment_status_id)
						@case(1)
							<p>Down Payment<p>
							@break
						@case(2)
							<p>Fully Paid</p>
							@break
						@case(3)
							<p>Cancelled</p>
							@break
						@default
							<p>Pending</p>
					@endswitch					
				</td>
				
				<td>
					<button type="button" class="btn btn-primary btn-confirm-payment" data-toggle="modal" data-target="#confirm_payment_modal" data-id="{{$user->id}}" data-name="{{$user->name}}">
							Confirm
					</button>
					<!-- <button class="btn btn-primary mt-2 btn-block">confirm</button> -->
				</td>
				
				

				

					
				</tr>
			
		    @endforeach
			
		  </tbody>
		</table>
		
	</div>
	
</div>


<!-- confirm payment MODAL -->
<div class="modal fade" id="confirm_payment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Comfirm Payment Received</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="POST" id="paymentConfirmation_modal_form">
				@csrf
				{{ method_field("PUT")}}
				<div class="modal-body">
					Please select payment type made by <b><span id="user_name"></span></b>: 
					
					<select class="custom-select" name="payment_status" 
					id="status">
						@foreach($payment_statuses as $payment_status)
						<option value="{{ $payment_status->id }}"
						{{ $payment_status->name == $user->payment_status 
						? "selected" : "" }}
						>
						{{ $payment_status->name}}
						</option>
						@endforeach
					</select>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					
					
					<button class="btn btn-primary">
						confirm
					</button>
					
				</div>
			</form>
		</div>
	</div>
</div>
@endif
@endsection