@extends("layouts.app")
@section("content")
<div class="row">
<h2>{{$title}}</h2>
</div>
<div class="row">
    <div class="col-9">
        <!-- SEARCH -->
        <form method="POST" class="form-group form-group-lg my-2" id="search-form">
            @csrf
            <input class="form-control mr-sm-2 " type="search" placeholder="Search Event" aria-label="Search" name="search" id="search">
        </form>
        <!-- Filter -->
        <form action="/" method = "GET" class = "d-flex flex-row">
            <!-- Filter by Category -->
            <div class="form-group flex-fill mr-1">
                <select name="category" id="" class="custom-select">
                    <option>Filter by category.....</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">
                        {{$old_category == $category->id ? "selected" : "" }}
                        
                        {{$category->name}}
                    </option>
                    @endforeach
                </select>
            </div>
            <!-- submit button -->
            <div class="form-group mr-1">
                <button class="btn btn-primary">Submit</button>
            </div>
            <!-- reset -->
            <div class="form-group">
                <a href="/" class = "btn btn-secondary">Reset</a>
            </div>
            </form>
    </div>
</div>
<div class="row">
    <div class="col-12 col-lg-9 mb-4 mb-lg-0 event-container">
        @foreach($events as $event)
        <div class="row event-row mt-3">

            <div class="col-5">
                <img src="{{$event['image']}}" class="card-img img-fluid welcome-image" alt="...">
            </div>

            <div class="col-6 card event-details">

                <div class="card-body">
                    <h5 class="card-title">{{$event['name']}}</h5>
                    <div>
                        <strong >Location:</strong> 
                        <p class="d-inline">{{$event['location']}}</p>
                    </div>
                    <div>
                        <strong>Date:</strong> 
                        <p class="d-inline">{{$event['date']}}</p>
                    </div>
                    <div>
                        <strong>Time:</strong>
                        <p class="d-inline">{{$event['time']}}</p>
                    </div>
                    <div>
                        <strong>Price:</strong> 
                        <p class="d-inline">&#8369; {{$event['Price']}}</p>                    
                     </div>
                     <!-- <div>
                        <strong>Target Slots:</strong> 
                            <p class="d-inline"> {{--$event['target_slots']--}}</p>
                    </div>
                    <div>
                        <strong>Remaining Slots:</strong> 
                        <p class="d-inline"> {{--$event['']--}}</p>
                    </div> -->
                    <div>
                        <strong>Status:</strong> 

                        {{$event->eventStatus->name }}

                    </div>
                    <div class="mb-2">
                        <strong>Category:</strong> 

                        {{ $event->category->name }}

                    </div>
                    <div class="mb-2">
                        <strong>Created By:</strong> 
                        {{ $event->owner->name }}

                    </div>


        




                </div>
                <a href="/event/{{ $event->id }}/view" class="btn btn-outline-primary my-1">View</a>
                @if(Auth::user() === null ||  Auth::user()->role_id==3)
                <a href="/event/{{ $event->id }}/participant" class="btn btn-outline-primary my-2">Join</a>
                @endif
            </div>



        </div>
        @endforeach
        
        {{ $events->links() }}
    </div>
    
    
    <div class="col-12 col-lg-3 ">
        <div class="col blog-container">
            <h4 class="text-center">Mountain Blogs</h4>
          
            <div class="card-deck mt-2">
                <div class="card">
                <a href="http://www.pinoymountaineer.com/2011/08/mt-damastraverse-685-in-san-clemente.html"><img src="{{asset('images/damas.jpg')}}" alt="..." class="img-thumbnail img-fluid blog-image"></a>
                <h5 class="card-title text-center">Mt. Damas</h5>            
                </div>
            </div>

            <div class="card-deck mt-2">
                <div class="card">
                <a href="http://www.pinoymountaineer.com/2019/09/hiking-matters-632-mt-amagi-%e5%a4%a9%e5%9f%8e%e5%b1%b1-the-highest-peak-in-izu-peninsula-japan.html"><img src="{{asset('images/amagi.jpg')}}" alt="..." class="img-thumbnail img-fluid blog-image"></a>
                <h5 class="card-title text-center">Mt. Amagi</h5>            
                </div>
            </div>

            <div class="card-deck mt-2">
                <div class="card">
                <a href="http://www.pinoymountaineer.com/2019/08/hiking-matters-629-mt-kiamo-pitcher-plant-mountain-in-malaybalay-bukidnon.html"><img src="{{asset('images/kiamo.jpg')}}" alt="..." class="img-thumbnail img-fluid blog-image"></a>
                <h5 class="card-title text-center">Mt. Kiamo</h5>            
                </div>
            </div>

            <div class="card-deck mt-2">
                <div class="card">
                <a href="http://www.pinoymountaineer.com/2019/05/hiking-matters-622-the-26-km-mt-ugo-dayhike-via-tinongdan.html"><img src="{{asset('images/ugo.jpg')}}" alt="..." class="img-thumbnail img-fluid blog-image"></a>
                <h5 class="card-title text-center">Mt. Ugo</h5>            
                </div>
            </div>
 
            <div class="card-deck mt-2">
                <div class="card">
                <a href="http://www.pinoymountaineer.com/2018/06/akiki-ambangeg-dayhike-pulag.html"><img src="{{asset('images/pulag.jpg')}}" alt="..." class="img-thumbnail img-fluid blog-image"></a>
                <h5 class="card-title text-center">Mt. Pulag</h5>            
                </div>
            </div>

            <div class="card-deck mt-2">
                <div class="card">
                <a href="http://www.pinoymountaineer.com/2017/12/hiking-matters-526-mt-taranaki-highest-in-north-island-new-zealand.html"><img src="{{asset('images/taranaki.jpg')}}" alt="..." class="img-thumbnail img-fluid blog-image"></a>
                <h5 class="card-title text-center">Mt.Taranaki</h5>            
                </div>
            </div>
        </div>
                    
	</div>
</div>
  
   
        


            

    
@endsection