@extends("layouts.app")
@section("content")

<div class="col">
<h2>{{$title}}</h2>
		<table class="table table-striped">
		  <thead>
		    <tr>
			
		      <th scope="col" width="20%">#</th>
		      <th scope="col" width="20%">Name</th>
		      <th scope="col" width="20%">Location</th>
		      <th scope="col" width="20%">Date</th>
		      <th scope="col" width="20%">Time</th>
		      <th scope="col" width="20%">Price</th>
              <th scope="col" width="20%">Payment Mode</th>
		      <th scope="col" width="20%">Status</th>
              <th scope="col" width="20%"></th>
		      
		    
		    </tr>
		  </thead>
		  <tbody>
		  @if(empty($events))
			{{-- dd($users) --}}
               <h2 class = "mt-5">You have not join any event yet...</h2>
			@else
		  
		    <tr>
			@foreach($events as $event)
              {{-- dd($transaction) --}}
			  <!-- <th>
			  <button type="button" class="btn btn-close btn-delete-event" data-toggle="modal" data-target="#delete_event_modal" data-id="" data-name="">
					 x
			  </button>
			  
			  </th> -->
		      <th scope="row">{{ $loop->iteration }}</th>
		      <td>{{$event->name}}</td>
		      <td>{{$event->location}}</td>
		      <td>{{$event->date}}</td>
		      <td>{{$event->time}}</td>
		      <td>{{$event->Price}}</td>
		      <td>{{$event->payment_mode}}</td>
		      <td> {{--($event->payment_status)--}}
			  		@if($event->payment_status != "Pending" && $event->payment_status != "Cancelled")
						confirmed
					@else
					{{$event->payment_status}}
					@endif  		
			  </td>
		      <td></td>

		     

		     

		      	
		    </tr>
		    @endforeach
			
		  </tbody>
		</table>
		
	</div>
	
</div>	

@endif
@endsection