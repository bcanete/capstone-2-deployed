@extends("layouts.app")

@section("content")
<h1>Create Event</h1>
	<form method="POST" enctype="multipart/form-data" action="/event/createSubmitted">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">Event Name:</label>
			<input type="text" name="name" placeholder="Name of Mountain or Place" class="form-control">
		</div>
		<div class="form-group">
			<label for="location">Location:</label>
			<input type="text" name="location" placeholder="location" class="form-control">
		</div>
		<div class="form-group">
			<label for="date">Date:</label>
			<input type="date" name="date" class="form-control">
		</div>
		<div class="form-group">
			<label for="time">Time:</label>
			<input type="time" name="time" placeholder="HH-MM-SS" class="form-control">
		</div>
		<div class="form-group">
			<label for="price">Price:</label>
			<input type="text" name="price" placeholder="price" class="form-control" step="0.01">
		</div>
		<div class="form-group">
			<label for="target-slots">Target Slots:</label>
			<input type="number" name="target_slots" placeholder="Target Slots" class="form-control">
		</div>
		<div class="form-group">
			<label for="image">Upload Image:</label>
			<div class="custom-file">
			  <input type="file" class="custom-file-input" id="image" name="image">
			  <label class="custom-file-label" for="customFile">Choose file</label>
		</div>
		</div>
		<div class="form-group">
			<label for="category">Category:</label>
			<select name="category">
				<option value="">--Please choose category--</option>
				<option value="1">Hiking/Treking</option>
				<option value="2">Beach</option>
				<option value="3">Tour</option>

			</select>
		</div>
		
		<div class="form-group">
			
			<label for="details">Details:</label><br>
			<small>Note: Please include Payment option details such as bank account number, Gcash, etc.</small>
			<textarea name="details" placeholder="details" id="details" class="form-control"></textarea>
		</div>

		<button class="btn btn-primary">Create Event</button>
	</form>
@endsection