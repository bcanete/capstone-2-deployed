@extends("layouts.app")
@section("content")
	
	

	<div class="row">
		<div class="col">
			<h2>{{$title}}</h2>
		</div>
	</div>
	{{--dd(Auth::user())--}}
	<div class="row">
		<div class="col-6">
			<div class="form-group">
				<label for="name">Event Name:</label>
				<input type="text" name="name" placeholder="Name of Mountain or Place" class="form-control" value="{{$event->name}}" disabled>
			</div>
			<div class="form-group">
				<label for="location">Location:</label>
				<input type="text" name="location" placeholder="location" class="form-control" value="{{$event->location}}" disabled>
			</div>
			<div class="form-group">
				<label for="date">Date:</label>
				<input type="text" name="date" placeholder="date" class="form-control" value="{{$event->date}}" disabled>
			</div>
			<div class="form-group">
				<label for="time">Time:</label>
				<input type="time" name="time" placeholder="time" class="form-control" value="{{$event->time}}" disabled>
			</div>
			<div class="form-group">
				<label for="price">Price:</label>
				<input type="text" name="price" placeholder="price" class="form-control" value="{{$event->Price}}" disabled> 
			</div>
			<div class="form-group">
				<label for="target-slots">Target Slots:</label>
				<input type="text" name="target-slots" placeholder="Target Slots" class="form-control" value="{{$event->time}}" disabled>
			</div>
			
			<div class="form-group">
				<label for="category">Category:</label>
				<input type="text" name="category" value="{{$event->category->name}}" class="form-control" disabled>
				
			</div>
			<div class="form-group">
				<label for="Status">Status:</label>
				<input type="text" name="status" value="{{$event->eventStatus->name}}" class="form-control" disabled>
				
			</div>
			<div class="form-group">
				<label>Details:</label>
				<details>
					<summary>View Detials</summary>
					<pre>{{$event->details}}</pre>
				</details>
				
			</div>

		</div>
		<div class="col-6">
			<img src="{{ $event->image }}" class="img-fluid">
			@if(Auth::user() == null|| Auth::user()->role_id == 3  )
			<a href="/event/{{ $event->id }}/participant" class="btn btn-primary btn-block mt-3" >Join</a>
			
			@elseif(Auth::user()->role_id != 1 || Auth::user()->role_id !=3)
		      	<a class="btn btn-primary btn-block mt-3" href="/event/{{ $event->id }}/edit" title="Edit">
				  edit
				</a>
			
			@endif
			
		</div>


	</div>		
	

		

		
	



@endsection