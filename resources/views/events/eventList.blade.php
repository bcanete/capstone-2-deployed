@extends("layouts.app")
@section("content")

<div class="col">
<h2>{{$title}}</h2>
@if(Auth::user()->role_id != 3)
<a href="/event/create">
	<i class="far fa-plus-square" name="create"></i>
</a>
@endif

<label for="create">Create Event</label>

		<table class="table table-striped">
		  <thead>
		    <tr>
			<th scope="col"></th>
		      <th scope="col" width="20%">#</th>
		      <th scope="col" width="20%">Name</th>
		      <th scope="col" width="20%">Location</th>
		      <th scope="col" width="20%">Date</th>
		      <th scope="col" width="20%">Time</th>
		      <th scope="col" width="20%">Price</th>
		      <!-- <th scope="col" width="20%">Target Slots</th> -->
		      <th scope="col" width="20%">Category</th>
		      <th scope="col" width="20%">Status</th>
		      <th scope="col" width="20%">Actions</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($events as $event)
		    <tr>
			  <th>
			  <button type="button" class="btn btn-close btn-delete-event" data-toggle="modal" data-target="#delete_event_modal" data-id="{{$event->id}}" data-name="{{$event->name}}">
					 x
			  </button>
			  
			  </th>
		      <th scope="row">{{ $loop->iteration }}</th>
		      <td>{{ $event->name }}</td>
		      <td>{{ $event->location }}</td>
		      <td>{{ $event->date }}</td>
		      <td>{{ $event->time }}</td>
		      <td>{{ $event->Price }}</td>
		      <!-- <td>{{-- $event->target_slots --}}</td> -->
		      <td>{{ $event->category->name }}</td>
		      <td>{{ $event->eventStatus->name }}</td>

		      <td class="d-flex justify-content-around">
		      	<a href="/event/{{ $event->id }}/view" class="btn btn-outline-primary"  title="View"><i class="fas fa-binoculars"></i></a>
				  @if(Auth::user()->role_id != 1)
		      	<a class="btn btn-outline-info " href="/event/{{ $event->id }}/edit" title="Edit"><i class="fas fa-edit"></i>
				</a>
				@endif
				<a href="/event/{{ $event->id }}/participantsList" class="btn btn-outline-info" title="Participants List"><i class="fas fa-list"></i></a>

		      </td>

		     

		      	
		    </tr>
		    @endforeach
			
		  </tbody>
		</table>
		{{$events->links()}}
	</div>
	
</div>	

<!-- DELETE PRODUCT MODAL -->
<div class="modal fade" id="delete_event_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Delete Event</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Do you want to delete <span id="delete_event_name"></span>?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<form method="POST" id="delete_modal_form">
				@csrf
				{{ method_field("DELETE") }}
				<button class="btn btn-danger">
					Delete
				</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection