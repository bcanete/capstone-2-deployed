@extends("layouts.app")

@section("content")
<div class="row">
		<div class="col">
			<h2>{{$title}}</h2>
		</div>
	</div>
	<div class="row">
		
			<div class="col-6">
			<form method="POST" enctype="multipart/form-data" action="/event/{{$events->id}}">
				@csrf
				{{method_field("PUT")}}
				<div class="form-group">
						<label for="name">Event Name:</label>
						<input type="text" name="name" placeholder="Name of Mountain or Place" class="form-control" value="{{$events->name}}" >
					</div>
					<div class="form-group">
						<label for="location">Location:</label>
						<input type="text" name="location" placeholder="location" class="form-control" value="{{$events->location}}" >
					</div>
					<div class="form-group">
						<label for="date">Date:</label>
						<input type="text" name="date" placeholder="date" class="form-control" value="{{$events->date}}" >
					</div>
					<div class="form-group">
						<label for="time">Time:</label>
						<input type="time" name="time" placeholder="time" class="form-control" value="{{$events->time}}" >
					</div>
					<div class="form-group">
						<label for="price">Price:</label>
						<input type="text" name="price" placeholder="price" class="form-control" value="{{$events->Price}}" > 
					</div>
					<div class="form-group">
						<label for="target-slots">Target Slots:</label>
						<input type="text" name="target-slots" placeholder="Target Slots" class="form-control" value="{{$events->target_slots}}" >
					</div>
					
					<div class="form-group">
						<label for="category">Category: </label>
							<select class="custom-select" name="category" 
							id="category">
								@foreach($categories as $category)
									<option value="{{ $category->id }}"
											{{ $category->id == $events->category_id 
												? "selected" : "" }}
										>
										{{ $category->name }}
									</option>
								@endforeach
							</select>
						
					</div>
					<div class="form-group">
					<label for="category">Status: </label>
							<select class="custom-select" name="status" 
							id="status">
								@foreach($status as $status)
									<option value="{{ $status->id }}"
											{{ $status->id == $events->status_id 
												? "selected" : "" }}
										>
										{{ $status->name }}
									</option>
								@endforeach
							</select>
						
					</div>
											
					<div class="form-group">
						<label for="details">Details:</label>
						<textarea name="details" placeholder="details" id="details" class="form-control" value=""><pre>{{$events->details}}</pre></textarea>
					</div>
					<div class="form-group">
													
					</div>
					

					
				
			</div>
			<div class="col-6 m">
					<img src="{{ $events->image }}" class="img-fluid">
						<div class="mt-2"	>
							<label for="image">Upload Image:</label>
							<div class="custom-file">
							<input type="file" class="custom-file-input" id="image" name="image">
							<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
			</div>
			<button class="btn btn-primary mt-2 btn-block">Save</button>
		</form>									

	</div>
@endsection