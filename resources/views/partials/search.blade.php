@foreach($events as $event)
    <div class="row mb-5 p-3">
            
            <div class="col-4">
              <img src="{{$event['image']}}" class="card-img img-fluid" alt="..." style="height:300px">
            </div>

            <div class="col-6 card">

                <div class="card-body">
                    <h5 class="card-title">{{$event['name']}}</h5>
                    <div>
                        <strong >Location:</strong> 
                        <p class="d-inline">{{$event['location']}}</p>
                    </div>
                    <div>
                        <strong>Date:</strong> 
                        <p class="d-inline">{{$event['date']}}</p>
                    </div>
                    <div>
                        <strong>Time:</strong>
                        <p class="d-inline">{{$event['time']}}</p>
                    </div>
                    <div>
                        <strong>Price:</strong> 
                        <p class="d-inline">&#8369; {{$event['Price']}}</p>                    
                    </div>
                    <div>
                        <strong>Target Slots:</strong> 
                        <p class="d-inline"> {{$event['target_slots']}}</p>
                    </div>
                    <div>
                        <strong>Remaining Slots:</strong> 
                        <p class="d-inline"> {{$event['']}}</p>
                    </div>
                     <div>
                        <strong>Status:</strong> 
                        
                            {{$event->eventStatus->name }}
                        
                    </div>
                    <div class="mb-2">
                        <strong>Category:</strong> 
                        
                        {{ $event->category->name }}
                            
                    </div>
                    <div class="mb-2">
                        <strong>Created By:</strong> 
                        {{ $event->owner->name }}
                            
                    </div>
                    
                    
                    <a href="/event/{{ $event->id }}/view" class="btn btn-outline-primary">View</a>

                    <a href="/event/{{ $event->id }}/participant" class="btn btn-outline-primary">Join</a>


                    

                </div>
            </div>
            

    </div>
@endforeach