<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_code');
            $table->timestamps();
            // $table->timestamps('created_at');
            // $table->timestamps('updated_at');
            $table->unsignedBigInteger('event_participants_id');
            $table->unsignedBigInteger('payment_mode_id');
            $table->unsignedBigInteger('payment_status_id');

            
            $table->foreign('event_participants_id')
            ->references('id')
            ->on('event_participants')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('payment_mode_id')
            ->references('id')
            ->on('payment_modes')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('payment_status_id')
            ->references('id')
            ->on('payment_statuses')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
