<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('location');
            $table->date('date');
            $table->time('time');
            $table->longText('details');
            $table->decimal('Price',8,2);
            $table->string('image')->default('https://via.placeholder.com/500.png');
            $table->integer('target_slots');
            $table->unsignedBigInteger('event_status_id')->default(1);
            $table->unsignedBigInteger('category_id');
            $table->timestamps();
            $table->unsignedBigInteger('user_id')->default(2);
            $table->softDeletes();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            // $table->timestamps();
            $table->foreign('event_status_id')
            ->references('id')
            ->on('event_statuses')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('category_id')
            ->references('id')
            ->on('categories')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
