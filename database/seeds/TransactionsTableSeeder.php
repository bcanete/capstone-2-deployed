<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'transaction_code' => 'uwQRh - 1576988340',
                'created_at' => '2019-12-22 04:19:00',
                'updated_at' => '2019-12-22 04:19:00',
                'event_participants_id' => 2,
                'payment_mode_id' => 4,
                'payment_status_id' => 4,
            ),
            1 => 
            array (
                'id' => 2,
                'transaction_code' => 'a1IGO - 1577049047',
                'created_at' => '2019-12-22 21:10:47',
                'updated_at' => '2019-12-22 21:10:47',
                'event_participants_id' => 3,
                'payment_mode_id' => 2,
                'payment_status_id' => 4,
            ),
            2 => 
            array (
                'id' => 3,
                'transaction_code' => '11zdG - 1577049732',
                'created_at' => '2019-12-22 21:22:12',
                'updated_at' => '2019-12-22 21:22:12',
                'event_participants_id' => 4,
                'payment_mode_id' => 4,
                'payment_status_id' => 4,
            ),
            3 => 
            array (
                'id' => 4,
                'transaction_code' => 'rU3uM - 1577138038',
                'created_at' => '2019-12-23 21:53:58',
                'updated_at' => '2019-12-23 23:00:04',
                'event_participants_id' => 5,
                'payment_mode_id' => 2,
                'payment_status_id' => 2,
            ),
            4 => 
            array (
                'id' => 5,
                'transaction_code' => 'u3y38 - 1577142786',
                'created_at' => '2019-12-23 23:13:06',
                'updated_at' => '2019-12-23 23:13:06',
                'event_participants_id' => 6,
                'payment_mode_id' => 2,
                'payment_status_id' => 4,
            ),
        ));
        
        
    }
}