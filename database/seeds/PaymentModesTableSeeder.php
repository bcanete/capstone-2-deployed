<?php

use Illuminate\Database\Seeder;

class PaymentModesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_modes')->delete();
        
        \DB::table('payment_modes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Bank Transfer',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Gcash',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'LBC',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Others',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}