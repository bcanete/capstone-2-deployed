<?php

use Illuminate\Database\Seeder;

class EventParticipantsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('event_participants')->delete();
        
        \DB::table('event_participants')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 4,
                'event_id' => 6,
                'created_at' => '2019-12-22 03:53:17',
                'updated_at' => '2019-12-22 03:53:17',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 4,
                'event_id' => 6,
                'created_at' => '2019-12-22 04:19:00',
                'updated_at' => '2019-12-22 04:19:00',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 2,
                'event_id' => 6,
                'created_at' => '2019-12-22 21:10:47',
                'updated_at' => '2019-12-22 21:10:47',
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 2,
                'event_id' => 10,
                'created_at' => '2019-12-22 21:22:12',
                'updated_at' => '2019-12-22 21:22:12',
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 2,
                'event_id' => 6,
                'created_at' => '2019-12-23 21:53:58',
                'updated_at' => '2019-12-23 21:53:58',
            ),
            5 => 
            array (
                'id' => 6,
                'user_id' => 4,
                'event_id' => 10,
                'created_at' => '2019-12-23 23:13:06',
                'updated_at' => '2019-12-23 23:13:06',
            ),
        ));
        
        
    }
}