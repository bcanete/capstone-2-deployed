<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PaymentStatusesTableSeeder::class);
        $this->call(EventStatusesTableSeeder::class);
        $this->call(PaymentModesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
        $this->call(EventParticipantsTableSeeder::class);
        
       
    }
}
