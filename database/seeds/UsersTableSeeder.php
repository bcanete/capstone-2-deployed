<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_name' => 'admin',
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$xE94PU5zvpvYfRuUu5KgNeiBQwWB2Fg9/nk2m0KCDwk6bw7890jkO',
                'contact_number' => '123456789',
                'role_id' => 1,
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'user_name' => 'ben10',
                'name' => 'bencan',
                'email' => 'ben@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$X1MzZNu4mA.hIx2GNC4NHOpuarIiSPrsuYGtrnygDbJtAJ9Km2uoS',
                'contact_number' => '123456789',
                'role_id' => 2,
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'user_name' => 'jon',
                'name' => 'jonsnow',
                'email' => 'jon@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$W47B8d6OmIdI6.aDyLjT.ODIUf7xS3KJFggU8AF8CBxssIv6Kzi6q',
                'contact_number' => '123456789',
                'role_id' => 3,
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'user_name' => 'user',
                'name' => 'user',
                'email' => 'user@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$nIRpv0E7hom5LbnSDdiBhuT98xFU.tT0ewxvKnsZ9QmcZGOgrj.wi',
                'contact_number' => '0917-545-8954',
                'role_id' => 3,
                'remember_token' => NULL,
                'created_at' => '2019-12-18 22:11:19',
                'updated_at' => '2019-12-18 22:11:19',
            ),
        ));
        
        
    }
}