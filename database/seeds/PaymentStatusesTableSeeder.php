<?php

use Illuminate\Database\Seeder;

class PaymentStatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_statuses')->delete();
        
        \DB::table('payment_statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Down Payment',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'FullyPaid',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Cancelled',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Pending',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}