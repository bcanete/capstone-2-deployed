<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('events')->delete();
        
        \DB::table('events')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Mt. Maculot',
                'location' => 'Cuenca, Batangas',
                'date' => '2019-12-26',
                'time' => '04:07:31',
                'details' => 'sasaddasdasdasdasdasdasdasdasddaasdasdasdasdasdasdasdasdaaaasssssssdfasfdsdasfwefasdfawefasdfasdfasdfasdfdsfsdafsadf',
                'Price' => '950.00',
                'image' => 'https://via.placeholder.com/500.png',
                'target_slots' => 20,
                'event_status_id' => 1,
                'category_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'user_id' => 2,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Mt. Bundok1',
                'location' => 'Tanay,Rizal',
                'date' => '2019-12-26',
                'time' => '04:07:31',
                'details' => 'sasaddasdasdasdasdasdasdasdasddaasdasdasdasdasdasdasdasdaaaasssssssdfasfdsdasfwefasdfawefasdfasdfasdfasdfdsfsdafsadf',
                'Price' => '950.00',
                'image' => 'https://via.placeholder.com/500.png',
                'target_slots' => 20,
                'event_status_id' => 1,
                'category_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'user_id' => 2,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Mt. Bundok2',
                'location' => 'Tanay,Rizal',
                'date' => '2019-12-26',
                'time' => '04:07:31',
                'details' => 'sasaddasdasdasdasdasdasdasdasddaasdasdasdasdasdasdasdasdaaaasssssssdfasfdsdasfwefasdfawefasdfasdfasdfasdfdsfsdafsadf',
                'Price' => '950.00',
                'image' => 'https://via.placeholder.com/500.png',
                'target_slots' => 20,
                'event_status_id' => 1,
                'category_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'user_id' => 2,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Mt. Bundok3',
                'location' => 'Tanay,Rizal',
                'date' => '2019-12-26',
                'time' => '04:07:31',
                'details' => 'sasaddasdasdasdasdasdasdasdasddaasdasdasdasdasdasdasdasdaaaasssssssdfasfdsdasfwefasdfawefasdfasdfasdfasdfdsfsdafsadf',
                'Price' => '950.00',
                'image' => 'https://via.placeholder.com/500.png',
                'target_slots' => 20,
                'event_status_id' => 1,
                'category_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'user_id' => 2,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Mt. Bundok4',
                'location' => 'Tanay,Rizal',
                'date' => '2019-12-26',
                'time' => '04:07:31',
                'details' => 'sasaddasdasdasdasdasdasdasdasddaasdasdasdasdasdasdasdasdaaaasssssssdfasfdsdasfwefasdfawefasdfasdfasdfasdfdsfsdafsadf',
                'Price' => '950.00',
                'image' => 'https://via.placeholder.com/500.png',
                'target_slots' => 20,
                'event_status_id' => 1,
                'category_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'user_id' => 2,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Laiya Beach',
                'location' => 'Tanay,Rizal',
                'date' => '2019-12-26',
                'time' => '04:07:31',
                'details' => 'sasaddasdasdasdasdasdasdasdasddaasdasdasdasdasdasdasdasdaaaasssssssdfasfdsdasfwefasdfawefasdfasdfasdfasdfdsfsdafsadf',
                'Price' => '950.00',
                'image' => 'https://via.placeholder.com/500.png',
                'target_slots' => 20,
                'event_status_id' => 1,
                'category_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'user_id' => 2,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'SASS CSS',
                'location' => 'asdsad',
                'date' => '2020-01-01',
                'time' => '14:01:00',
                'details' => '
asdasd

sad

sad

asd',
                'Price' => '2000.00',
                'image' => '/images//1576797786.png',
                'target_slots' => 2,
                'event_status_id' => 1,
                'category_id' => 1,
                'created_at' => '2019-12-19 23:23:06',
                'updated_at' => '2019-12-19 23:51:27',
                'user_id' => 2,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'sdasdaasdasdsad',
                'location' => 'Sto. Thomas, Batangas',
                'date' => '2020-01-01',
                'time' => '02:01:00',
                'details' => 'asdasd

asd

asd

asd

sd

&nbsp;',
                'Price' => '2900.00',
                'image' => '/images//1576799813.jpg',
                'target_slots' => 2,
                'event_status_id' => 1,
                'category_id' => 1,
                'created_at' => '2019-12-19 23:56:53',
                'updated_at' => '2019-12-21 19:15:10',
                'user_id' => 2,
                'deleted_at' => '2019-12-21 19:15:10',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Mt Pigingan',
                'location' => 'Itogon,Benguet',
                'date' => '2020-02-03',
                'time' => '01:00:00',
                'details' => 'asdas

asd

asd

asd

asd

asd

asd

asd

asd

&nbsp;',
                'Price' => '2000.00',
                'image' => '/images//1576799869.jpg',
                'target_slots' => 2,
                'event_status_id' => 1,
                'category_id' => 1,
                'created_at' => '2019-12-19 23:57:49',
                'updated_at' => '2019-12-19 23:57:49',
                'user_id' => 2,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Mt. Kupapey',
                'location' => 'asdasd',
                'date' => '2020-03-01',
                'time' => '02:00:00',
                'details' => 'asdasd

asd

asda

sdas

da

sd

asd

&nbsp;',
                'Price' => '100.00',
                'image' => '/images//1576799914.jpg',
                'target_slots' => 2,
                'event_status_id' => 1,
                'category_id' => 1,
                'created_at' => '2019-12-19 23:58:34',
                'updated_at' => '2019-12-19 23:58:34',
                'user_id' => 2,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}